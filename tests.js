const assert = require('./simpletests').assert

assert("bye or good bye", "bye" != "good bye")

assert("42 is 42", 42 == 42)

process.exit(0)